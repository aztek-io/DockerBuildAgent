########################################
### Image ##############################
########################################

image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

########################################
### Prerequisites ######################
########################################

before_script:
  - |
        #!/bin/sh
        set -e

        if command -v terraform; then
            terraform --version
            terraform init
        elif command -v shellcheck; then
            shellcheck --version
        fi

stages:
  - test_syntax
  - plan
  - apply
  - destroy

########################################
### Lint Bash Scripts ##################
########################################

Shellcheck:
  stage: test_syntax
  script:
    - |
        print (){
            var="$1";
            size="${#var}";
            printf -v sep "%${size}s" "-";
            printf "\n%s\n%s\n" "$var" "${sep// /-}"
        }

        find . -name "*.sh" > /tmp/bash_script.list

        while read -r BASH_SCRIPT; do
            print "$BASH_SCRIPT"
            shellcheck "$BASH_SCRIPT"

            if ! grep '^set -e' "$BASH_SCRIPT"; then
                echo ' - `set -e` not set in script. FAILING build.'
                exit 1
            fi

            echo " - All checks passed."
        done < /tmp/bash_script.list
  image: koalaman/shellcheck-alpine

########################################
### Validte ############################
########################################

TerraformValidate:
  stage: test_syntax
  script:
    - terraform validate

########################################
### Plan ###############################
########################################

TerraformPlan:
  stage: plan
  script:
    - terraform plan
  artifacts:
    name: plan

########################################
### Apply ##############################
########################################

TerraformApply:
  stage: apply
  environment:
    name: production
  script:
    - terraform apply -auto-approve
  dependencies:
    - TerraformPlan

########################################
### Clean Up ###########################
########################################

TerraformDestroy:
  stage: destroy
  script:
    - terraform destroy -force
  dependencies:
    - TerraformApply
  when: manual
