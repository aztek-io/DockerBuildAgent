########################################
### Variables ##########################
########################################

variable "global" {
    type = "map"
    default = {
        environment = "Staging"
        region      = "us-west-2"
    }
}

variable "vpc" {
    type = "map"
    default = {
        vpc_name_tag    = "ScriptMyJob VPC"
        subnet_name     = "Public Web Server"
        sg_name_ssh     = "SSH from trusted networks and Lambda"
    }
}

variable "ec2" {
    type = "map"
    default = {
        lc_name     = "DockerBuildAgent"
        asg_name    = "DockerBuildAgent ASG"
        image       = "ami-0def3275"
        size        = "t2.small"
        key_name    = "Staging"
        min_size    = 1
        max_size    = 1
        tag_name    = "Docker Build Agent"
    }
}

########################################
### AWS Data ###########################
########################################

data "aws_availability_zones" "available" {}

data "aws_vpc" "selected" {
    tags {
        Name = "${lookup(var.vpc,"vpc_name_tag")}"
    }
}

data "aws_subnet_ids" "public_web_server" {
    vpc_id  = "${data.aws_vpc.selected.id}"

    tags {
        Name = "${lookup(var.vpc,"subnet_name")}"
    }
}

data "aws_security_groups" "SSH" {
    tags {
        Name = "${lookup(var.vpc,"sg_name_ssh")}"
    }
}

