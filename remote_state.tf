terraform {
  backend "s3" {
    bucket  = "scriptmyjob.terraform.tfstate"
    key     = "DockerBuildAgent/terraform.tfstate"
    region  = "us-west-2"
    encrypt = "true"
  }
}

