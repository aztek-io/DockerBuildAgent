#!/usr/bin/env bash
set -ex

export DEBIAN_FRONTEND=noninteractive

Invoke-MainFunction(){
    Install-Dependancies
    Install-Docker
    Install-Terraform
    Install-GitLabRunner
    Configure-GitLabRunner "m6anwtbyjJyZgpJoGVeG"
}

Install-Dependancies(){
    sudo apt-get update -y

    sudo apt-get install -y \
        python \
        python-pip \
        lsof \
        unzip
}

Install-Docker(){
    sudo apt-get install -y \
        docker.io

    sudo systemctl start docker
}

Install-GitLabRunner(){
    sudo wget \
        -O /usr/local/bin/gitlab-runner \
        https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

    sudo chmod +x /usr/local/bin/gitlab-runner

    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

    sudo gitlab-runner install \
        --user=gitlab-runner \
        --working-directory=/home/gitlab-runner

    sudo gitlab-runner start
}

Configure-GitLabRunner(){
    GITLAB_TOKEN="$1"

    gitlab-runner register \
        --url https://gitlab.com \
        --registration-token "${GITLAB_TOKEN}" \
        --description "Docker Build Agent" \
        --tag-list "Shell,Docker,Ubuntu,Terraform" \
        --executor shell \
        --non-interactive

    sudo usermod -aG docker gitlab-runner

    sudo -u gitlab-runner -H docker info
}

Install-Terraform(){
    VERSION="$(Get-Version)"

    Download-Latest "$VERSION"
    Update-Path
}

Get-Version(){
    type -a jq > /dev/null || {
        apt-get install -y jq 1> /dev/null || {
            yum install -y jq 1> /dev/null || {
                exit 1
            }
        }
    }

    curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | \
        jq -r -M '.current_version'
}

Download-Latest(){
    VERSION="$1"
    rm -f /tmp/terraform_latest.zip || true
    wget -O /tmp/terraform_latest.zip "https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip"
}

Update-Path(){
    unzip /tmp/terraform_latest.zip -d /tmp
    rm -f /bin/terraform
    mv /tmp/terraform /bin
}

Invoke-MainFunction
