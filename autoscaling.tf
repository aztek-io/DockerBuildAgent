resource "aws_launch_configuration" "Docker_Build_Agent" {
    name_prefix                     = "${lookup(var.ec2, "lc_name")}"
    image_id                        = "${lookup(var.ec2, "image")}"
    instance_type                   = "${lookup(var.ec2, "size")}"
    key_name                        = "${lookup(var.ec2, "key_name")}"
    security_groups                 = [
        "${data.aws_security_groups.SSH.ids}"
    ]
    associate_public_ip_address     = true
    user_data                       = "${file("bootstrap.sh")}"

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "Docker_Build_Agent" {
    availability_zones              = [
        "${data.aws_availability_zones.available.names}"
    ]

    name                            = "${lookup(var.ec2, "asg_name")}"
    max_size                        = "${lookup(var.ec2, "max_size")}"
    min_size                        = "${lookup(var.ec2, "min_size")}"
    launch_configuration            = "${aws_launch_configuration.Docker_Build_Agent.name}"

    vpc_zone_identifier             = [
        "${data.aws_subnet_ids.public_web_server.ids}"
    ]

    tags    = [
        {
            key                     = "Name"
            value                   = "${lookup(var.ec2, "tag_name")}"
            propagate_at_launch     = true
        },
        {
            key                     = "Environment"
            value                   = "${lookup(var.global, "environment")}"
            propagate_at_launch     = true
        }
    ]
}
